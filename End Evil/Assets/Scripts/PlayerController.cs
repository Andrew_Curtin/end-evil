﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    public float speed = 1; //5
    public float rotSpeedX = 1; //3
    public float rotSpeedY = 1; //3
    public float upperVLimit = 25; //25
    public float lowerVLimit = -25;
    float verticalCounter;
    Camera viewCamera;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        viewCamera = Camera.main;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (h != 0 || v != 0)
        {
            Vector3 temp = transform.forward * v * speed + transform.right * h * speed;
            rb.velocity = new Vector3(temp.x, rb.velocity.y, temp.z);
            //rb.velocity = new Vector3(h * speed, rb.velocity.y, v * speed); //<-not relative to self
        }
        else
        {
            rb.velocity = Vector3.up * rb.velocity.y;
        }

        //horizontal, controls both camera and player
        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * rotSpeedX);
        }

        //vertical, with limiters
        //[INVERSE]
        /*if ((Input.GetAxis("Mouse Y") > 0 && verticalCounter < upperVLimit) ||
            (Input.GetAxis("Mouse Y") < 0 && verticalCounter > lowerVLimit))
        {
            viewCamera.transform.Rotate(Vector3.right * rotSpeedY * Input.GetAxis("Mouse Y"), Space.Self);
            verticalCounter += Input.GetAxis("Mouse Y");
        }*/

        if ((Input.GetAxis("Mouse Y") < 0 && verticalCounter < upperVLimit) ||
            (Input.GetAxis("Mouse Y") > 0 && verticalCounter > lowerVLimit))
        {
            viewCamera.transform.Rotate(Vector3.right * rotSpeedY * -Input.GetAxis("Mouse Y"), Space.Self);
            verticalCounter -= Input.GetAxis("Mouse Y");
        }

        //Debug.Log(verticalCounter);

        /*TO DO:
         * Slopes; player will launch up and down slopes rather than following along them
         * Stairs; need to be tested, could just treat them as slopes
         * Jump; run a ground check everytime the player tries to jump, use rays or collision layers
         * Firing; either this is done here or limited here and done in another script
         * Aiming down sights; input hold slow movement lerp camera and weapon into position return on release
         * Melee; stops movement, activates a collider and enemy inside gets pushed back a little
         * Sprint; limit turning, melee, and firing, uses up stamina
         * Grenade; spawns nade with its own script, trajectory based on viewCamera
         * 
         */
    }
}
